import QtQuick 2.4
import Lomiri.Components 1.3
import QtQuick.Layouts 1.1
import Lomiri.Components.ListItems 1.3
import Stopwatch 1.0

Page {
    id: root
    title: i18n.tr("Stopwatch")

    property date startTime: new Date()
    property date snapshot: startTime
    property bool running: false

    property int timeDiff: snapshot - startTime
    property int oldDiff: 0

    property int totalTimeDiff: timeDiff + oldDiff

    function start() {
        startTime = new Date();
        snapshot = startTime;
        running = true;
    }

    function stop() {
        oldDiff += timeDiff;
        startTime = new Date();
        snapshot = startTime;
        running = false;
    }

    function update() {
        snapshot = new Date();
    }

    function clear() {
        oldDiff = 0;
        startTime = new Date();
        snapshot = startTime;
        history.clear();
    }

    Timer {
        id: refreshTimer
        interval: 45
        repeat: true
        running: root.running

        onTriggered: {
            root.update();
        }
    }

    ColumnLayout {
        id: mainColumn
        anchors {
            fill: parent
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
            topMargin: units.gu(1)
        }
        spacing: units.gu(2)

        TimeDisplay {
            Layout.fillWidth: true
            milliseconds: root.totalTimeDiff
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: units.gu(2)

            Button {
                Layout.preferredWidth: (mainColumn.width - parent.spacing) / 2
                Layout.preferredHeight: units.gu(10)
                color: root.running ? LomiriColors.orange : LomiriColors.red
                font.pixelSize: units.gu(4)
                text: root.running ? i18n.tr("Stop") : i18n.tr("Clear")
                onClicked: {
                    if (root.running) {
                        root.stop();
                    } else {
                        root.clear();
                    }
                }
            }

            Button {
                Layout.preferredWidth: (mainColumn.width - parent.spacing) / 2
                Layout.preferredHeight: units.gu(10)
                color: root.running ? LomiriColors.blue : LomiriColors.green
                font.pixelSize: units.gu(4)

                text: root.running ? i18n.tr("Lap") : i18n.tr("Start")
                enabled: !root.running || historyView.count < 999

                onClicked: {
                    if (root.running) {
                        root.update();
                        history.addLap(root.totalTimeDiff);
                    } else {
                        root.start();
                    }
                }
            }
        }

        ListView {
            id: historyView
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: history
            clip: true
            highlightFollowsCurrentItem: true

            onCountChanged: {
                currentIndex = count - 1
            }

            delegate: Empty {
                Row {
                    anchors.fill: parent
                    anchors.topMargin: units.gu(1)
                    anchors.bottomMargin: units.gu(1)
                    spacing: units.gu(2)

                    Row {
                        spacing: units.gu(.5)
                        height: parent.height
                        width: height
                        property bool hundrets: index >= 99
                        Digit {
                            height: parent.height / 2
                            visible: parent.hundrets
                            anchors.verticalCenter: parent.verticalCenter
                            number: Math.floor((index + 1) / 100)
                        }
                        Digit {
                            height: parent.height / (parent.hundrets ? 2 : 1)
                            anchors.verticalCenter: parent.verticalCenter
                            number: Math.floor((index + 1) / 10) % 10
                        }
                        Digit {
                            height: parent.height / (parent.hundrets ? 2 : 1)
                            anchors.verticalCenter: parent.verticalCenter
                            number: (index + 1) % 10
                        }
                    }
                    GridLayout {
                        width: parent.width - parent.spacing - x
                        columns: 3
                        anchors.verticalCenter: parent.verticalCenter

                        Label {
                            Layout.preferredWidth: parent.width / 3
                            text: i18n.tr("Total")
                            color: LomiriColors.lightGrey
                        }
                        Label {
                            id: lapLabel
                            Layout.preferredWidth: parent.width / 3
                            color: LomiriColors.lightGrey
                            text: i18n.tr("Lap")
                        }
                        Item {
                            Layout.preferredWidth: parent.width / 3
                            Image {
                                height: lapLabel.height / 2
                                anchors.right: parent.right
                                width: height * 2.5
                                Layout.alignment: Qt.AlignRight
                                source: Qt.resolvedUrl("delta.svg")
                                visible: index > 0
                            }
                        }

                        TimeLabel {
                            Layout.fillWidth: true
                            millis: model.time
                        }
                        TimeLabel {
                            Layout.fillWidth: true
                            millis: model.diffToPrevious
                        }
                        TimeLabel {
                            Layout.fillWidth: true
                            color: model.delta == 0 ? "white" : model.delta < 0 ? LomiriColors.green : LomiriColors.red
                            millis: model.delta
                            showSign: true
                            rightAligned: true
                            visible: index > 0
                        }
                    }
                }
            }
        }
    }
}
