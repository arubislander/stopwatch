import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import Stopwatch 1.0

Page {
    id: root
    title: i18n.tr("Countdown")

    property bool running: false

    Alarm {
        id: alarm
    }

    property real buttonOpacity: 0
    Behavior on buttonOpacity {
        LomiriNumberAnimation {}
    }

    states: [
        State {
            name: "edit"; when: !alarm.running
            PropertyChanges { target: timeDisplay; showMillis: false }
            PropertyChanges { target: root; buttonOpacity: 1 }
        },
        State {
            name: "finished"; when: alarm.running && alarm.millis == 0
            PropertyChanges { target: blinkAnimation; running: true }
            PropertyChanges { target: timeDisplay; opacity: blinkAnimation.on ? 1 : 0 }
        }
    ]

    Timer {
        id: blinkAnimation
        repeat: true
        running: false
        interval: 500
        property bool on: true
        onTriggered: on = !on
    }

    ColumnLayout {
        id: mainColumn
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
            margins: units.gu(1)
        }
        height: units.gu(38)
        spacing: units.gu(2)

        RowLayout {
            Layout.fillWidth: true

            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addHours(10)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < 90 * 60 * 60 * 1000
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addHours(1)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < 99 * 60 * 60 * 1000
            }
            Item {
                Layout.preferredWidth: parent.width / 12
            }

            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addMinutes(10)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < (99 * 60 + 50) * 60 * 1000
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addMinutes(1)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < (99 * 60 + 59) * 60 * 1000
            }
            Item {
                Layout.preferredWidth: parent.width / 12
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addSeconds(10)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < ((99 * 60 + 59) * 60 + 50) * 1000
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addSeconds(1)
                iconSource: Qt.resolvedUrl("up.svg")
                opacity: root.buttonOpacity
                enabled: alarm.millis < ((99 * 60 + 59) * 60 + 59) * 1000
            }

        }

        TimeDisplay {
            Layout.fillWidth: true
            id: timeDisplay
            width: parent.width
            milliseconds: alarm.millis
        }

        RowLayout {
            Layout.fillWidth: true

            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addHours(-10)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 10 * 60 * 60 * 1000
                opacity: root.buttonOpacity
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addHours(-1)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 60 * 60 * 1000
                opacity: root.buttonOpacity
            }
            Item {
                Layout.preferredWidth: parent.width / 12
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addMinutes(-10)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 10 * 60 * 1000
                opacity: root.buttonOpacity
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addMinutes(-1)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 60 * 1000
                opacity: root.buttonOpacity
            }
            Item {
                Layout.preferredWidth: parent.width / 12
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addSeconds(-10)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 10 * 1000
                opacity: root.buttonOpacity
            }
            Button {
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: LomiriColors.darkGrey
                onClicked: alarm.addSeconds(-1)
                iconSource: Qt.resolvedUrl("down.svg")
                enabled: alarm.millis >= 1000
                opacity: root.buttonOpacity
            }
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: units.gu(2)

            Button {
                Layout.preferredWidth: (mainColumn.width - parent.spacing) / 2
                Layout.preferredHeight: units.gu(10)
                color: alarm.running ? LomiriColors.orange : LomiriColors.red
                font.pixelSize: units.gu(4)
                text: alarm.running ? i18n.tr("Stop") : i18n.tr("Clear")

                enabled: alarm.running || alarm.millis > 0
                onClicked: {
                    if (alarm.running) {
                        alarm.stop();
                    } else {
                        alarm.millis = 0;
                    }
                }
            }

            Button {
                Layout.preferredWidth: (mainColumn.width - parent.spacing) / 2
                Layout.preferredHeight: units.gu(10)
                color: alarm.running && !alarm.paused ? LomiriColors.blue : LomiriColors.green
                font.pixelSize: units.gu(4)

                text: alarm.running && !alarm.paused ? i18n.tr("Pause") : i18n.tr("Start")
                enabled: alarm.millis > 0

                onClicked: {
                    if (alarm.running && !alarm.paused) {
                        alarm.pause();
                    } else {
                        alarm.start();
                    }
                }
            }
        }
    }
}
