import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

RowLayout {
    id: root
    property int millis: 0
    property color color: "white"
    property bool showSign: false
    property bool rightAligned: false

    function millisToString(millis, showMilliseconds) {
        var milliseconds = millis % 1000;
        var seconds = Math.floor(millis / 1000) % 60;
        var minutes = Math.floor(millis / 1000 / 60) % 60
        var hours = Math.floor(millis / 1000 / 60 / 60)
        var result = "";
        if (hours > 0) {
            result += hours + ":"
        }
        result += zeroPadding(minutes, 2) + ":";
        result += zeroPadding(seconds, 2) + ".";
        if (showMilliseconds) {
            result += zeroPadding(milliseconds, 3)
        }
        return result;
    }

    function zeroPadding(str, count) {
        var string = "" + str;
        var tmp = ""
        for (var i = 0; i < count; i++) {
            tmp += "0"
        }
        return (tmp + str).substring(string.length)
    }

    Item {
        Layout.fillWidth: root.rightAligned
    }

    Row {
        Label {
            //model.delta >= 0 ? "+" + root.timeDiffToString(model.delta) : "-" + root.timeDiffToString(-model.delta)
            color: root.color
            property string sign: millis >= 0 ? "+" : "-"
            text: (root.showSign ? sign : "") + (millis >= 0 ? millisToString(millis) : millisToString(-millis))
            anchors.bottom: parent.bottom
        }
        Label {
            text: zeroPadding("" + ((millis >= 0 ? millis : -millis) % 1000), 3)
            fontSize: "x-small"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: units.dp(1)
            color: root.color
        }
    }
    Item {
        Layout.fillWidth: !root.rightAligned
    }
}
