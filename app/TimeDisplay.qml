/*****************************************************************************
 * Copyright: 2015 Michael Zanetti <michael_zanetti@gmx.net>                 *
 *                                                                           *
 * This prject is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This project is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 ****************************************************************************/

import QtQuick 2.4
import QtQuick.Layouts 1.1
import Lomiri.Components 1.3

Row {
    id: root
    spacing: height / 10
    height: baseHeight

    property int milliseconds: 0

    property int digits: 5
    property bool showMillis: true
    property real scaleFactor: showMillis ? 6 : 4.15
    property int baseHeight: width / scaleFactor
    Behavior on baseHeight {
        LomiriNumberAnimation {}
    }

    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000 / 60 / 60 / 10) % 10
    }
    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000 / 60 / 60) % 10
    }
    Digit {
        height: root.baseHeight
        number: ":"
    }
    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000 / 60 / 10) % 6
    }
    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000 / 60) % 10
    }
    Digit {
        height: root.baseHeight
        number: ":"
    }
    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000 / 10) % 6
    }
    Digit {
        height: root.baseHeight
        number: Math.floor(root.milliseconds / 1000) % 10
    }
    Digit {
        height: root.baseHeight
        number: "."
    }
    Digit {
        height: root.baseHeight * 0.75
        number: Math.floor(root.milliseconds / 100) % 10
        anchors.bottom: parent.bottom
    }
    Digit {
        height: root.baseHeight * 0.75
        number: Math.floor(root.milliseconds / 10) % 10
        anchors.bottom: parent.bottom
    }
    Digit {
        height: root.height * 0.75
        number: Math.floor(root.milliseconds) % 10
        anchors.bottom: parent.bottom
    }
}
