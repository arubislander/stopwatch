TEMPLATE = app
TARGET = app

load(ubuntu-click)

QT += qml quick organizer

SOURCES += main.cpp \
    history.cpp \
    alarm.cpp

RESOURCES += app.qrc

OTHER_FILES += app.apparmor \
               app.desktop \
               icon.svg

#specify where the config files are installed to
config_files.path = /app
config_files.files += $${OTHER_FILES}
message($$config_files.files)
INSTALLS+=config_files

# Default rules for deployment.
target.path = $${UBUNTU_CLICK_BINARY_PATH}
INSTALLS+=target

HEADERS += \
    history.h \
    alarm.h

