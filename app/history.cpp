/*****************************************************************************
 * Copyright: 2015 Michael Zanetti <michael_zanetti@gmx.net>                 *
 *                                                                           *
 * This file is part of ubuntu-authenticator                                 *
 *                                                                           *
 * This prject is free software: you can redistribute it and/or modify       *
 * it under the terms of the GNU General Public License as published by      *
 * the Free Software Foundation, either version 3 of the License, or         *
 * (at your option) any later version.                                       *
 *                                                                           *
 * This project is distributed in the hope that it will be useful,           *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
 * GNU General Public License for more details.                              *
 *                                                                           *
 * You should have received a copy of the GNU General Public License         *
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.     *
 *                                                                           *
 ****************************************************************************/

#include "history.h"

#include <QStandardPaths>
#include <iostream>

History::History(QObject *parent) :
    QAbstractListModel(parent),
    m_settings(QStandardPaths::standardLocations(QStandardPaths::ConfigLocation).first() + "/stopwatch.arubislander/stopwatch.arubislander.conf", QSettings::IniFormat)
{
    std::cout << "loading laps" << m_settings.fileName().toStdString() << std::endl;
}

int History::rowCount(const QModelIndex &parent) const
{
    return m_settings.value("laps").toList().count();
}

QVariant History::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case RoleTime:
        return m_settings.value("laps").toList().at(index.row());
    case RoleDiffToPrevious: {
        int previous = index.row() == 0 ? 0 : data(this->index(index.row() - 1), RoleTime).toInt();
        return m_settings.value("laps").toList().at(index.row()).toInt() - previous;
    }
    case RoleDelta: {
        int previousTimeDiff = index.row() == 0 ? 0 : data(this->index(index.row() - 1), RoleDiffToPrevious).toInt();
        int thisTimeDiff = data(this->index(index.row()), RoleDiffToPrevious).toInt();
        return thisTimeDiff - previousTimeDiff;
    }
    }
    return QVariant();
}

QHash<int, QByteArray> History::roleNames() const
{
    QHash< int, QByteArray> roles;
    roles.insert(RoleTime, "time");
    roles.insert(RoleDiffToPrevious, "diffToPrevious");
    roles.insert(RoleDelta, "delta");
    return roles;
}

void History::addLap(int timeDiff)
{
    QVariantList laps = m_settings.value("laps").toList();
    beginInsertRows(QModelIndex(), laps.count(), laps.count());
    laps.append(timeDiff);
    m_settings.setValue("laps", laps);
    endInsertRows();
}

void History::clear()
{
    beginResetModel();
    m_settings.setValue("laps", QVariantList());
    endResetModel();
}
