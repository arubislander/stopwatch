#include "alarm.h"

#include <QOrganizerTodo>
#include <QOrganizerItemVisualReminder>
#include <QOrganizerItemAudibleReminder>
#include <QOrganizerItemSaveRequest>
#include <QOrganizerItemFetchRequest>
#include <QOrganizerItemRemoveRequest>
#include <QOrganizerItemCollectionFilter>
#include <QOrganizerTodoTime>

QTORGANIZER_USE_NAMESPACE

#define ALARM_MANAGER           "eds"
#define ALARM_MANAGER_FALLBACK  "memory"
#define ALARM_COLLECTION        "Stopwatch"

Alarm::Alarm(QObject *parent) :
    m_millis(0),
    QObject(parent),
    m_busy(false),
    m_restart(false),
    m_running(false)
{

    QString envManager(qgetenv("ALARM_BACKEND"));
    if (envManager.isEmpty())
        envManager = ALARM_MANAGER;
    if (!QOrganizerManager::availableManagers().contains(envManager)) {
        envManager = ALARM_MANAGER_FALLBACK;
    }
    m_manager = new QOrganizerManager(envManager);
    m_manager->setParent(this);

    QList<QOrganizerCollection> collections = m_manager->collections();
    if (collections.count() > 0) {
        Q_FOREACH(const QOrganizerCollection &c, collections) {
            if (c.metaData(QOrganizerCollection::KeyName).toString() == ALARM_COLLECTION) {
                m_collection = c;
                break;
            }
        }
    }
    if (m_collection.id().isNull()) {
        // create alarm collection
        m_collection.setMetaData(QOrganizerCollection::KeyName, ALARM_COLLECTION);
        // EDS requires extra metadata to be set
        m_collection.setExtendedMetaData("collection-type", "Task List");
        if (!m_manager->saveCollection(&m_collection)) {
//            qDebug() << "WARNING: Creating dedicated collection for reminders was not possible, reminders will be saved into the default collection!";
            m_collection = m_manager->collection(m_manager->defaultCollectionId());
        }
    }

    connect(&m_timer, &QTimer::timeout, this, &Alarm::update);
}

int Alarm::millis() const
{
    return m_millis;
}

void Alarm::setMillis(int millis)
{
    if (m_millis != millis) {
        m_millis = millis;
        emit millisChanged();
    }
}

bool Alarm::running() const
{
    return m_running;
}

bool Alarm::paused() const
{
    return !m_timer.isActive();
}


#include <QDebug>

void Alarm::addSeconds(int seconds)
{
    m_millis = qMax(0, m_millis + seconds * 1000);
    emit millisChanged();
}

void Alarm::addMinutes(int minutes)
{
    m_millis = qMax(0, m_millis + minutes * 60 * 1000);
    emit millisChanged();
}

void Alarm::addHours(int hours)
{
    m_millis = qMax(0, m_millis + hours * 60 * 60 * 1000);
    emit millisChanged();
}

void Alarm::start()
{
    qDebug() << "starting";
    m_startTime = QDateTime::currentDateTime();
    m_timer.start(45);
    startSync();
    m_running = true;
    emit runningChanged();
    void pausedChanged();
}

void Alarm::stop()
{
    if (!paused()) {
        update();
        startSync();
        m_timer.stop();
    }
    m_running = false;
    emit runningChanged();
    void pausedChanged();
}

void Alarm::pause()
{
    update();
    startSync();
    m_timer.stop();
    emit pausedChanged();
}

void Alarm::startSync()
{
    if (m_busy) {
        m_restart = true;
        return;
    }
    m_restart = false;
    m_busy = true;
    loadAlarms();
}

void Alarm::setupAlarm()
{
    QOrganizerTodo item;
    item.setCollectionId(m_collection.id());
    item.setAllDay(false);
    item.setStartDateTime(m_startTime.toUTC().addMSecs(m_millis));
    item.setDisplayLabel("Countdown");
    item.setDescription("blabla");

    QOrganizerItemVisualReminder visual;
    visual.setSecondsBeforeStart(0);
    visual.setMessage("Countdown finished");
    item.saveDetail(&visual);

    QOrganizerItemAudibleReminder audible;
    audible.setSecondsBeforeStart(0);
    //audible.setDataUrl(alarm.sound);
    item.saveDetail(&audible);

    QOrganizerItemSaveRequest *operation = new QOrganizerItemSaveRequest(this);
    operation->setManager(m_manager);
    operation->setItem(item);
    connect(operation, &QOrganizerItemFetchRequest::stateChanged, this, &Alarm::writeStateChanged);
    operation->start();
}

void Alarm::loadAlarms()
{
    QOrganizerItemFetchRequest *operation = new QOrganizerItemFetchRequest(this);
    operation->setManager(m_manager);

    // set sort order
    QOrganizerItemSortOrder sortOrder;
    sortOrder.setDirection(Qt::AscendingOrder);
    sortOrder.setDetail(QOrganizerItemDetail::TypeTodoTime, QOrganizerTodoTime::FieldStartDateTime);
    operation->setSorting(QList<QOrganizerItemSortOrder>() << sortOrder);

    // set filter
    QOrganizerItemCollectionFilter filter;
    filter.setCollectionId(m_collection.id());
    operation->setFilter(filter);

    // start request
    connect(operation, &QOrganizerItemFetchRequest::stateChanged, this, &Alarm::fetchStateChanged);
    operation->start();
}

void Alarm::fetchStateChanged(QOrganizerAbstractRequest::State state)
{
    QOrganizerItemFetchRequest *request = static_cast<QOrganizerItemFetchRequest*>(sender());

    if (m_restart) {
        m_busy = false;
        startSync();
        return;
    }

    if (state == QOrganizerAbstractRequest::CanceledState) {
        qDebug() << "Error syncing reminders. Could not read organizer items.";
        m_busy = false;
        request->deleteLater();
        return;
    }

    // cleaning up old reminders
    if (state == QOrganizerAbstractRequest::FinishedState) {
         QList<QOrganizerItem> items = request->items();
         foreach (const QOrganizerItem &item, items) {
             QOrganizerItemRemoveRequest *removeRequest = new QOrganizerItemRemoveRequest(this);
             removeRequest->setItem(item);
             removeRequest->setManager(m_manager);
             connect(removeRequest, &QOrganizerItemRemoveRequest::stateChanged, this, &Alarm::deleteStateChanged);
             removeRequest->start();
         }
         request->deleteLater();
         if (m_timer.isActive()) {
             setupAlarm();
         }
         m_busy = false;
    }
}

void Alarm::deleteStateChanged(QOrganizerAbstractRequest::State state)
{
    QOrganizerItemSaveRequest *request = static_cast<QOrganizerItemSaveRequest*>(sender());
    if (state == QOrganizerAbstractRequest::FinishedState || state == QOrganizerAbstractRequest::CanceledState) {
        request->deleteLater();
    }
}

void Alarm::writeStateChanged(QOrganizerAbstractRequest::State state)
{
    QOrganizerItemSaveRequest *request = static_cast<QOrganizerItemSaveRequest*>(sender());
    if (state == QOrganizerAbstractRequest::FinishedState || state == QOrganizerAbstractRequest::CanceledState) {
        request->deleteLater();
    }
}

void Alarm::update()
{
    QDateTime now = QDateTime::currentDateTime();
    int elapsed = now.toMSecsSinceEpoch() - m_startTime.toMSecsSinceEpoch();
    m_startTime = now;
    m_millis = qMax(0, m_millis - elapsed);
    emit millisChanged();
    if (m_millis == 0) {
        m_timer.stop();
    }
}
